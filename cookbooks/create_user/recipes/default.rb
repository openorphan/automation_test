#
# Cookbook Name:: create_user
# Recipe:: default
#
# Copyright 2018, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
directory "/automation" do
    action :create
end

user 'autoadmin' do
  supports :manage_home => true
  comment 'For Operation team'
  home '/home/autoadmin'
  shell '/bin/bash'
  password '$1$q8loaD69$Y5DVUlPphMGS.cIDxYTEO1'
end

